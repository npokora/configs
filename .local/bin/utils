#!/usr/bin/env sh

DEFAULT_LIMIT="$(ulimit -s)"
ulimit -s 81922

working_dirs_weights="cd"

cd_weighted () { builtin cd $@; sexec wlist $working_dirs_weights increase "$PWD"; }
cd_path () { [[ -d "$1" ]] && cd_weighted "$1"; }

working_dirs () { sexec wlist $working_dirs_weights get; }

files () { fd -t f "$@"; }
files_sort () { files "$@" --exec-batch stat -c "%Y %n" | sort -r | cut -d ' ' -f 2-; }
files_hidden () { files -H "$@"; }
files_hidden_sort () { files_hidden "$@" --exec-batch stat -c "%Y %n" | sort -r | cut -d ' ' -f 2-; }

dirs () { fd -t d "$@"; }
dirs_sort () { dirs "$@" --exec-batch stat -c "%Y %n" | sort -r | cut -d ' ' -f 2-; }
dirs_hidden () { dirs -H "$@"; }
dirs_hidden_sort () { dirs_hidden "$@" --exec-batch stat -c "%Y %n" | sort -r | cut -d ' ' -f 2-; }

git_files () { git ls-tree --name-only -r "$(git branch --show-current)"; }

files_select () { files | mselect; }
files_hidden_select () { files_hidden | mselect; }
files_hidden_sort_select () { files_hidden_sort | MSELECT_SORT=true mselect; }

working_dirs_files_hidden_select () { working_dirs_cd && files_hidden_select | xargs -r -I {} printf "$(pwd)/%s" "{}"; }

files_open () { files_select | xargs -d '\n' -r fedit; }
files_hidden_open () { files_hidden_select | xargs -d '\n' -r fedit; }
files_sort_open () { files_sort | MSELECT_SORT=true mselect | xargs -d '\n' -r fedit; }

working_dirs_files_hidden_open () { working_dirs_files_hidden_select | xargs -d '\n' -r fedit; }

files_cat () { files_select | xargs -d '\n' -r cat; }
files_sort_cat () { files_sort | MSELECT_SORT=true mselect | xargs -d '\n' -r cat; }

files_explore () { files_select | xargs -d '\n' -r fexplore; }
files_sort_explore () { files_sort | MSELECT_SORT=true mselect | xargs -d '\n' -r fexplore; }

dirs_open () { dirs | mselect | xargs -d '\n' -r fedit; }
dirs_sort_open () { dirs_sort | MSELECT_SORT=true mselect | xargs -d '\n' -r fedit; }

dirs_explore () { dirs | mselect | xargs -d '\n' -r fexplore; }
dirs_sort_explore () { dirs_sort | MSELECT_SORT=true mselect | xargs -d '\n' -r fexplore; }

dirs_cd () { cd_path "$(dirs | mselect)"; }
dirs_hidden_sort_cd () { cd_path "$(dirs_hidden_sort | MSELECT_SORT=true mselect)"; }

working_dirs_cd () { cd_path "$(working_dirs | mselect)"; }

git_files_select () { git_files | mselect; }
git_files_open () { git_files_select | xargs -d '\n' -r fedit; }

AG_ARGS="--noheading --nobreak --color --column --ignore .git --ignore node_modules"

search () { xargs -d '\n' -r ag $AG_ARGS "$@" 2> /dev/null; }
search_files () { files | search "$@"; }
search_git_files () { git_files | search "$@"; }
search_hidden_files () { files_hidden | search "$@"; }

search_files_select () { search_files "$@" | mselect; }
search_git_files_select () { search_git_files "$@" | mselect; }
search_hidden_files_select () { search_hidden_files "$@" | mselect; }

search_files_select_filename () { search_files_select "$@" | awk -F ':' '{ print "\""$1"\"" }'; }
search_hidden_files_select_filename () { search_hidden_files_select "$@" | awk -F ':' '{ print "\""$1"\"" }'; }

search_files_open () { search_files_select "$@" | awk -F ':' '{ print "\""$1"\" "$2" "$3 }' | xargs -r fedit; }
search_git_files_open () { search_git_files_select "$@" | awk -F ':' '{ print "\""$1"\" "$2" "$3 }' | xargs -r fedit; }
search_hidden_files_open () { search_hidden_files_select "$@" | awk -F ':' '{ print "\""$1"\" "$2" "$3 }' | xargs -r fedit; }

search_files_explore () { search_files_select_filename "$@" | xargs -r fexplore; }
search_hidden_files_explore () { search_hidden_files_select_filename "$@" | xargs -r fexplore; }

search_git_files_kak () { search_git_files --nocolor "$@" > /tmp/git_kak && kak -e "set window filetype grep; cd $PWD" /tmp/git_kak; }

# option=$(echo "find git
# find git-open
# find fzf
# find hidden-fzf
# find hidden-sort-fzf
# find explore
# find hidden-explore
# find hidden-sort-explore
# find open
# find hidden-open
# find hidden-sort-open
# find cd
# find hidden-cd
# find hidden-sort-cd
# find cat
# find hidden-cat
# find hidden-sort-cat
# find less
# find hidden-less
# find hidden-sort-less
# find hidden
# find nocolor
# find sort
# find hn
# find hs" | fzf)

# action=$(printf "%s" "$option" | awk '{ print $1 }')
# subaction=$(printf "%s" "$option" | awk '{ print $2 }')
# echo "$action"
# echo "$subaction"

if type $1 &>/dev/null; then
  $1 "${@:2}"
  ulimit -s "$DEFAULT_LIMIT"
else
  echo "function not found"
  exit 1
fi
