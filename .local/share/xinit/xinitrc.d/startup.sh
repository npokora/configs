#!/usr/bin/env sh

dunst &
dxhd &

sexec wallpaper-switcher.sh &
# feh --no-fehbg --bg-fill $XDG_DATA_HOME/wallpapers/candy-dreams.jpg

picom --experimental-backends &

# sexec status-bars/polybar.sh &
sexec status-bars/lemonbar.sh &
xdo below -t $(xdo id -n root) $(xdo id -a 'lemonbar')

xss-lock -n "sexec dim-screen.sh" -- sexec i3-lock-with-blur.sh &

stalonetray --config $XDG_CONFIG_HOME/stalonetray/stalonetrayrc &

flameshot &
# blueman-applet &
# nm-applet &
