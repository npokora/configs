#/bin/sh

TMUX_CONF="$XDG_CONFIG_HOME/tmux/tmux.conf"
TMUX_TPM="$XDG_DATA_HOME/tmux/plugins/tpm"

echo "setting up tmux:"
if ! [ -d $TMUX_TPM ]; then
  git clone https://github.com/tmux-plugins/tpm $TMUX_TPM
  echo "- TPM installed"
else
  echo "- TPM already installed"
fi


TMUX_PLUGIN_MANAGER_PATH="$XDG_DATA_HOME/tmux/plugins/"

sh $TMUX_TPM/scripts/install_plugins.sh
sh $TMUX_TPM/scripts/clean_plugins.sh
sh $TMUX_TPM/scripts/update_plugin.sh --tmux-echo all
echo "- TPM plugins updated"

tmux source-file $TMUX_CONF
echo "- tmux config sourced"
