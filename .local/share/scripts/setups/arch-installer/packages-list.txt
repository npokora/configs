# monitors
arandr

# audio
audacity
cmus

# background image
feh


# tray
stalonetray

# windows manager
bspwm

# keybinding
sxhkd

# search/replace
awk
sed
the_silver_searcher
fd
fzf

# web
qutebrowser
w3m

# for now just to improve qutebrowser startup
socat

# terminal
alacritty

# file manager
vifm

# editor
kakoune
gvim

# terminal multiplexer
tmux


# notification
dunst


# games
lutris

# source-highlight
source-highlight

# X tools
xdo
xsel

# translator
translate-shell

#fonts
ttf-dejavu
ttf-liberation
noto-fonts

# application toolkits
qt5-base
gtk2
gtk3

alsa
alsa-utils
alsa-plugins
alsa-oss
alsa-firmware
blueman
networkmanager
network-manager-applet
bash-completion
xdotool
wmctrl
pulseaudio
pulseaudio-alsa
pulseaudio-bluetooth
pulseaudio-jack
ctags
jq
pass
sudo
base-devel
go
networkmanager-openvpn
nm-connection-editor
xorg
xorg-xinit
pavucontrol
bashtop
flameshot
xss-lock

zathura
zathura-pdf-mupdf
stow
tor
transmission-gtk
chafa
arc-gtk-theme

acpilight
