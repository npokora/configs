#!/usr/bin/env sh

get () {
  while read info; do
    node="$(printf "%s" "$info" | cut -d ' ' -f 1)"
    description="$(printf "%s" "$info" | cut -d ' ' -f 2-) $node"

    case "$(sexec get-current-wm.sh)" in
      "bspwm")
        if bspc query -N -n $node.hidden > /dev/null; then
          description="○ $description"
        else
          description="● $description"
        fi
        # if bspc query -N -n $node.focused > /dev/null; then
        #   description="* $description"
        # else
        #   description="- $description"
        # fi
      ;;
    esac

    printf "%s\n" "$description"
  done <<< "$(wmctrl -l | cut -d ' ' -f 1,3,5-)"

}

run () {
  local node="$(printf "%s" "$1" | rev | cut -d ' ' -f 1 | rev)"

  case "$(sexec get-current-wm.sh)" in
    "bspwm") bspc node $node -g hidden=off -f $node;;
  esac
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
