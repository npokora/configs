#!/usr/bin/env sh

get () {
  ducks=(
    "!g"
    "!yt"
    "!gtuk"
  )

  printf "%s\n" "${ducks[@]}"
}

run () {
  local query="$1"

  bexec xdg-open "https://duckduckgo.com?q=$query"
}

menu () {
  menu_output="$(get | MSELECT_QUERY=true mselect)"

  user_input="$(echo "$menu_output" | awk 'NR==1')"
  item="$(echo "$menu_output" | awk 'NR==2')"

  query="$user_input"

  for duck in "${ducks[@]}"; do
    if [[ "$item" == "$duck" ]] || [[ "$user_input" == "$duck" ]]; then
      query=$(echo "$duck " | wexec fedit - 1 'last')
    fi
  done

  run "$query"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
