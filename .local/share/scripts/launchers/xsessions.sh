#!/usr/bin/env sh

get () {
  find /usr/share/xsessions/ -type f -printf "%f\n" | awk -F '.desktop' ' { print $1}'
}

run () {
  local xsession="$1"
  local execLine="$(grep '^Exec' /usr/share/xsessions/$xsession.desktop | tail -1)"
  local command="$(printf "%s" "$execLine" | sed 's/^Exec=//' | sed 's/%.//' | sed 's/^"//g' | sed 's/" *$//g')"

  # exec $command
  echo "This is not currently working"
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
