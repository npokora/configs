#!/usr/bin/env sh

get () {
  sessions=$(sexec jdm list-others | awk '{ print "switch "$1" "substr($2, 4) }')

  [[ -n "$sessions" ]] && printf '%s\n' "${sessions[@]}"
  printf '%s' "create"
}

run () {
  local command="$1"

  sexec jdm $command
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
