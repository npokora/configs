#!/usr/bin/env sh

get () {
  applications=$(ls --color=no "$SYSTEM_SHARE_DIR/applications"; ls --color=no "$XDG_DATA_HOME/applications")
  printf "%s" "$applications" | awk -F '.desktop$' ' { print $1}'
}

run () {
  local app="$1"

  bexec gtk-launch "$app"
}

menu () {
  command="$(get | mselect)"
  run "$command"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
