#!/usr/bin/env sh

plugin_map="{
  \"actions\": [\"sexec launchers/actions.sh get\", \"sexec launchers/actions.sh run\"],
  \"jdm\": [\"sexec launchers/jdm.sh get\", \"sexec launchers/jdm.sh run\"],
  \"app\": [\"sexec launchers/applications.sh get\", \"sexec launchers/applications.sh run\"],
  \"notes\": [\"sexec launchers/notes.sh get\", \"sexec launchers/notes.sh run\"],
  \"configs\": [\"sexec launchers/configs.sh get\", \"sexec launchers/configs.sh run\"],
  \"command\": [\"sexec launchers/commands.sh get\", \"sexec launchers/commands.sh run\"],
  \"command-detached\": [\"sexec launchers/commands.sh get\", \"sexec launchers/commands.sh run-detached\"],
  \"launchers-scripts\": [\"sexec launchers/launchers-scripts.sh get\", \"sexec launchers/launchers-scripts.sh run\"],
  \"pass\": [\"sexec launchers/pass.sh get\", \"sexec launchers/pass.sh run\"],
  \"duckduckgo\": [\"sexec launchers/duckduckgo.sh get\", \"sexec launchers/duckduckgo.sh run\"]
}"

weight_profile='launcher'
launcher_cache=/tmp/$USER/launcher
mkdir -p $launcher_cache

plugin_get () {
  local plugin_name="$1"
  local plugin_script=`printf "%s" "$plugin_map" | jq -r ".\"$plugin_name\"[0]"`

  $plugin_script | awk "{ print length, \$0\"[$plugin_name]\"}" | sort -n | cut -d" " -f2-
}

plugin_run () {
  local plugin_name="$1"
  local item="$(printf "%s" "$2" | sed "s/\[$plugin_name\]\$//")"
  local plugin_script="$(printf "$plugin_map" | jq -r ".\"$plugin_name\"[1]")"

  $plugin_script "$item"
}

get_items () {
  plugin_get actions
  # plugin_get jdm
  plugin_get app
  plugin_get launchers-scripts
  plugin_get pass
  plugin_get notes
  # plugin_get command
  plugin_get configs
  plugin_get command-detached
  # plugin_get duckduckgo
}

get_cached_items () {
  local cache_file="$launcher_cache/items"
  local cache_file_tmp="$launcher_cache/items_tmp"

  if [ -f "$cache_file" ]; then
    cat "$cache_file"
  fi

  get_items > $cache_file_tmp

  local new_entries="$(cat $cache_file_tmp | cat $cache_file - | sort | uniq -u)"

  echo "$new_entries"

  cat "$cache_file_tmp" > $cache_file
}

get_list () {
  sexec wlist $weight_profile get && get_cached_items
}

get () {
  get_list | awk '!seen[$0]++'
}

run () {
  local item_with_weight="$1"

  item=`printf "%s" "$item_with_weight" | sed 's/\[[0-9]\+\]$//'`

  [[ -z "$item" ]] && return

  sexec wlist $weight_profile increase "$item"
  case "$item" in
    *"[actions]") plugin_run actions "$item";;
    *"[jdm]") plugin_run jdm "$item";;
    *"[app]") plugin_run app "$item";;
    *"[command]") plugin_run command "$item";;
    *"[command-detached]") plugin_run command-detached "$item";;
    *"[launchers-scripts]") plugin_run launchers-scripts "$item";;
    *"[pass]") plugin_run pass "$item";;
    *"[notes]") plugin_run notes "$item";;
    *"[configs]") plugin_run configs "$item";;
    *"[duckduckgo]") plugin_run duckduckgo "$item";;
  esac
}

run_multiple () {
  local items="$1"

  printf "%s\n" "$items" | while read item_with_weight; do
    run "$item_with_weight"
  done
}

menu () {
  # get_cached_items | while read item; do
  #   echo "$(get_weight "$item") $item"
  # done
  menu_output="$(get | MSELECT_QUERY=true mselect)"
  user_input="$(printf "%s" "$menu_output" | awk 'NR==1')"
  items="$(printf "%s" "$menu_output" | awk 'NR!=1')"

  run_multiple "$items"
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
  "run-multiple") run_multiple "$2";;
esac
