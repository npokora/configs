#!/usr/bin/env sh

get () {
  compgen -c | sort -u
}

run () {
  local cmd="$1"
  local user_input="$2"

  if [[ "$user_input" == "$cmd"* ]]; then
    cmd="$user_input"
  fi

  bexec wexec "$cmd; bash"
}

run_detached () {
  local cmd="$1"
  local user_input="$2"

  if [[ "$user_input" == "$cmd"* ]]; then
    cmd="$user_input"
  fi

  bexec $cmd
}

menu () {
  command="$(get | MSELECT_QUERY=true mselect)"
  run $command
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

menu_detached () {
  command="$(get | MSELECT_QUERY=true mselect)"
  run_detached $command
}

window_detached () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu;-else || menu-detached; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "menu-detached") menu_detached;;
  "window-detached") window_detached;;
  "run") run "$2" "$3";;
  "run-detached") run_detached "$2" "$3";;
esac
