#!/usr/bin/env sh

get () {
  UNKNOWN="$(psstatus unknown)"
  KNOWN_BAD="$(psstatus known-bad)"
  NL=$'\n'
  ALL="$UNKNOWN"
  [ ! -z "$ALL" ] && ALL="$ALL$NL$KNOWN_BAD" || ALL="$KNOWN_BAD"

  printf "%s" "$ALL"
}

run () {
  kill -9 $@
}

menu () {
  command="$(get | mselect | awk '{print $2}')"
  run $command
}

window () {
  if [[ "$MENU" == "fzf" ]]; then wexec $0 menu; else menu; fi
}

case "$1" in
  "get") get;;
  "menu") menu;;
  "window") window;;
  "run") run "$2";;
esac
