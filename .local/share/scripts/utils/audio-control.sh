#!/usr/bin/env sh

action="$1"

volume_mute_get () {
  DATA=$(amixer get Master | grep 'Left:')
  printf "%s" "$DATA" | awk -F'[][]' '{ print $4 }'
}

volume_get () {
  DATA=$(amixer get Master | grep 'Left:')
  printf "%s" "$DATA" | awk -F'[][]' '{ print substr($2, 1, length($2) - 1) }'
}

volume_change () {
  CHANGE="$1"
  CURRENT_VOLUME="$(volume_get)"
  VOLUME=$(( CURRENT_VOLUME + CHANGE ))

  (( VOLUME < 0 )) && VOLUME="0"

  amixer set Master "$VOLUME%"
}

[[ "$action" == "volume-toggle" ]] && amixer --quiet set Master toggle

[[ "$action" == "volume-change" ]] && volume_change "$2"

[[ "$action" == "volume-get" ]] && volume_get

[[ "$action" == "volume-mute-get" ]] && volume_mute_get

exit 0
