#!/usr/bin/env sh

tmp_dir=/tmp/$USER
mkdir -p $tmp_dir

#!#!/usr/bin/env bash

# Example locker script -- demonstrates how to use the --transfer-sleep-lock
# option with i3lock's forking mode to delay sleep until the screen is locked.

## CONFIGURATION ##############################################################


B='#00000000'  # blank
C='#ffffff00'  # clear ish
D='#9eff37'  # default
DD='#356700'  # default dark
G='#a2a2a2'  # grey
W='#d00202'  # wrong

# Options to pass to i3lock
run_i3lock () {
  i3lock-color -i $tmp_dir/screen.png \
    --insidevercolor=$C --ringvercolor=$D --insidewrongcolor=$C --ringwrongcolor=$W --insidecolor=$B \
    --ringcolor=$D --linecolor=$B --separatorcolor=$D --verifcolor=$D --wrongcolor=$W --timecolor=$D \
    --datecolor=$D --layoutcolor=$G --keyhlcolor=$DD --bshlcolor=$W \
    --clock --timestr='%H:%M:%S' --datestr='%A' \
    --veriftext='...' --wrongtext='Навіщо ти тут?' --noinputtext='Пусто' --wrongsize=18 \
    --keylayout 1 --layoutpos='w/2:h-20' --indicator "$@"

}

# Run before starting the locker
pre_lock() {
    dunstctl set-paused true

    scrot "$tmp_dir/screen.png"
    convert -sample 5% -scale 2000% "$tmp_dir/screen.png" "$tmp_dir/screen.png"
    xset s 3 86400

    return
}

# Run after the locker exits
post_lock() {
    xset s $XSET_S_OPTIONS
    rm "$tmp_dir/screen.png"

    dunstctl set-paused false
    return
}

###############################################################################

pre_lock

# We set a trap to kill the locker if we get killed, then start the locker and
# wait for it to exit. The waiting is not that straightforward when the locker
# forks, so we use this polling only if we have a sleep lock to deal with.
if [[ -e /dev/fd/${XSS_SLEEP_LOCK_FD:--1} ]]; then
    kill_i3lock() {
        pkill -xu $EUID "$@" i3lock-color
    }

    trap kill_i3lock TERM INT

    # we have to make sure the locker does not inherit a copy of the lock fd
    run_i3lock {XSS_SLEEP_LOCK_FD}<&-

    # now close our fd (only remaining copy) to indicate we're ready to sleep
    exec {XSS_SLEEP_LOCK_FD}<&-

    while kill_i3lock -0; do
        sleep 0.5
    done
else
    trap 'kill %%' TERM INT
    run_i3lock -n &
    wait
fi

post_lock

