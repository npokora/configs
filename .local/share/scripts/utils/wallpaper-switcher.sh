#!/usr/bin/env sh

WALLPAPERS_DIR="$XDG_DATA_HOME/wallpapers"

while true; do
  wallpaper="$(ls $WALLPAPERS_DIR | shuf -n 1)"
  [[ ! -z "$wallpaper" ]] && feh --bg-fill $WALLPAPERS_DIR/$wallpaper
  sleep 6000;
done
