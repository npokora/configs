#!/usr/bin/env sh

VOLUME=$(sexec audio-control.sh volume-get)
MUTE=$(sexec audio-control.sh volume-mute-get)

if [ "$MUTE" == "off" ]; then
  printf "%s" ""
else
  printf "%s \u2009" "$VOLUME%"
fi
