#!/usr/bin/env sh

UNKNOWN="$(psstatus unknown)"
KNOWN_BAD_REG="$(psstatus known-bad-reg)"

[ -z "$UNKNOWN" ] && UNKNOWN_COUNT='0' || UNKNOWN_COUNT="$(printf "%s\n" "$UNKNOWN" | wc -l)"
[ -z "$KNOWN_BAD_REG" ] && KNOWN_BAD_REG_COUNT='0' || KNOWN_BAD_REG_COUNT="$(printf "%s\n" "$KNOWN_BAD_REG" | wc -l)"

printf "%s \u2009$STATUS_BAR_DELIMITER%s \u2009" "$UNKNOWN_COUNT" "$KNOWN_BAD_REG_COUNT"
