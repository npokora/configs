#!/usr/bin/env sh

STATE=""

add () {
  if [ -z "$STATE" ]; then
    STATE="$@"
  else
    STATE="$STATE$STATUS_BAR_DELIMITER$@"
  fi
}

while read connection; do
  NM_CONNECTION_NAME="$(printf "%s" "$connection" | awk '{ print $1 }' | cut -c -4)"
  NM_CONNECTION_DEVICE="$(printf "%s" "$connection" | awk '{ print $4 }')"
  NM_CONNECTION_TYPE="$(printf "%s" "$connection" | awk '{ print $3 }')"

  if [[ "$NM_CONNECTION_TYPE" == "wifi" ]]; then
    add `printf "%s \uf1eb\u200a" "$NM_CONNECTION_NAME"`
  elif [[ "$NM_CONNECTION_TYPE" == "vpn" ]]; then
    add `printf "%s \uf3ed\u2009" "$NM_CONNECTION_NAME"`
  elif [[ "$connection" != "" ]]; then
    add `printf "%s \uf796\u2009" "$NM_CONNECTION_DEVICE"`
  fi
done <<< "$(nmcli connection show --active | awk '/(vpn|wifi)/')"

printf "%s" "$STATE"
