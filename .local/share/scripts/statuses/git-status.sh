#!/usr/bin/env sh

p () {
  printf "%s" "$1"
}

BRANCH=`git branch --show-current`

RESULT=""

[[ ! -z "$BRANCH" ]] && RESULT="${RESULT}$( p "${BRANCH}" | cut -c -15)" || exit 1;

STATUS=`git status --porcelain`
DIFF_STATUS=`git diff --shortstat`
DIFF_STAGED_STATUS=`git diff --staged --shortstat`
STASH=`git stash list | wc -l`

UNTRACKED=`p "$STATUS" | awk '/^\s*\?/' | wc -l`
# MODIFIED=`p "$STATUS" | awk '/^\s*M/' | wc -l`
DELETED=`p "$STATUS" | awk '/^\s*D/' | wc -l`
RENAMED=`p "$STATUS" | awk '/^\s*R/' | wc -l`

MODIFIED=`p "$DIFF_STATUS" | awk -F ' ' '{print $1}'`
MODIFIED_STAGED=`p "$DIFF_STAGED_STATUS" | awk -F ' ' '{print $1}'`
STAGED=`p "$DIFF_STAGED_STATUS" | awk -F ' ' '{print $1}'`

DIVERGED=`git rev-list --left-right --count $BRANCH...origin/$BRANCH`

AHEAD=`p "$DIVERGED" | awk -F ' ' '{print $1}'`
BEHIND=`p "$DIVERGED" | awk -F ' ' '{print $2}'`

RESULT_STATE=""
[[ ! -z "$STASH" && "$STASH" != "0" ]] && RESULT_STATE="${RESULT_STATE}# "
[[ ! -z "$DELETED" && "$DELETED" != "0" ]] && RESULT_STATE="${RESULT_STATE}‍${DELETED}-"
[[ ! -z "$RENAMED" && "$RENAMED" != "0" ]] && RESULT_STATE="${RESULT_STATE}‍${RENAMED}>"
[[ ! -z "$MODIFIED" && "$MODIFIED" != "0" ]] && RESULT_STATE="${RESULT_STATE}${MODIFIED}!"
[[ ! -z "$UNTRACKED" && "$UNTRACKED" != "0" ]] && RESULT_STATE="${RESULT_STATE}${UNTRACKED}?"
[[ ! -z "$STAGED" && "$STAGED" != "0" ]] && RESULT_STATE="${RESULT_STATE} ${STAGED}+ "
[[ ! -z "$AHEAD" && "$AHEAD" != "0" ]] && RESULT_STATE="${RESULT_STATE}  ${AHEAD}⇡ "
[[ ! -z "$BEHIND" && "$BEHIND" != "0" ]] && RESULT_STATE="${RESULT_STATE}${BEHIND}⇣ "

[[ ! -z "$RESULT_STATE" ]] && RESULT="$RESULT[$(p "$RESULT_STATE" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' -e 's/[[:space:]]\+/ /')]"
[[ ! -z "$RESULT" ]] && p "$RESULT"

# [git_status]
# conflicted = "🏳"
# # ahead = "🏎💨"
# # behind = "😰"
# # diverged = "😵"
# untracked = "‍ "
# stashed = "📦"
# modified = ""
# staged = '[+$count](green)'
# renamed = " "
# # deleted = "🗑 "
# # deleted = "X"
# deleted = " "
# ahead = "⇡${count}"
# diverged = "⇕⇡${ahead_count}⇣${behind_count}"
# behind = "⇣${count}"
# format = '([$all_status$ahead_behind]($style) )'

# UNTRACKED=""
