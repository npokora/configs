#!/usr/bin/env sh

if [[ ! -d "/sys/class/power_supply/*" ]]; then
  printf "\uf240"
  exit
fi

BATTERY_THRESHOLD="$1"
BATTERY_CAPACITY="$(cat /sys/class/power_supply/*/capacity)"
BATTERY_STATUS="$(cat /sys/class/power_supply/*/status)"

[[ ! -z "$BATTERY_THRESHOLD" ]] && (( BATTERY_THRESHOLD < BATTERY_CAPACITY )) && exit 0

[[ "$BATTERY_STATUS" == "Discharging" ]] && STATUS="\uf0d7" || STATUS="\uf0d8"
# printf "%s" "B: $BATTERY_CAPACITY%"
[[ $BATTERY_CAPACITY == "100" ]] && printf "\uf240" || printf "%s \uf242 $STATUS\u200a" "$BATTERY_CAPACITY%"

if [[ "$BATTERY_CAPACITY" -eq 5 ]]; then
  notify-send -u critical "Battery level is lower then 5"
elif [[ "$BATTERY_CAPACITY" -eq 10 ]]; then
  notify-send "Battery level is lower then 10"
fi
