#!/usr/bin/env sh

STATUS_BAR_WIDTH="$(printf "%s" "$(sexec monitors.sh dimentions)" | awk -F '[x/+]' 'NR==1 {print $1}')"
STATUS_BAR_HEIGHT="14"

sexec status-bars/status-bar.sh \
  | dzen2 -p -fn "Font Awesome;UbuntuMono Nerd Font" \
    -w "$(( STATUS_BAR_WIDTH - 7 ))"
    -h "$STATUS_BAR_HEIGHT" \
    -x "3" \
    -y "3"
