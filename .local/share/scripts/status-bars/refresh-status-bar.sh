#!/usr/bin/env sh

printf 'SPOTIFY%s\n' "$(sexec statuses/spotify.py)" > "$STATUS_BAR_FIFO" &
printf 'CMUS%s\n' "$(sexec statuses/cmus.sh)" > "$STATUS_BAR_FIFO" &
# printf 'NORDVPN%s\n' "$(sexec statuses/nordvpn.sh)" > "$STATUS_BAR_FIFO" &
printf 'VOLUME%s\n' "$(sexec statuses/volume.sh)" > "$STATUS_BAR_FIFO" &
printf 'PROCESSES%s\n' "$(sexec statuses/processes.sh)" > "$STATUS_BAR_FIFO" &
printf 'BATTERY%s\n' "$(sexec statuses/battery.sh)" > "$STATUS_BAR_FIFO" &
# printf 'ARCH_UPDATES%s\n' "$(sexec statuses/arch-updates.sh)" > "$STATUS_BAR_FIFO" &
printf 'NETWORK%s\n' "$(sexec statuses/network.sh)" > "$STATUS_BAR_FIFO" &
printf 'KEYBOARD_LAYOUT%s\n' "$(sexec statuses/keyboard-layout.sh)" > "$STATUS_BAR_FIFO" &
printf 'HIDDEN_WINDOWS%s\n' "$(sexec statuses/hidden-windows.sh)" > "$STATUS_BAR_FIFO" &


