#!/usr/bin/env sh

repeat () {
  local slogan="$1"
  local cmd="$2"
  local repeat="$3"

  while true; do
    local output="$(bash -c "$cmd")"
    
    pn "$slogan$output"

    sleep $repeat
  done
}

p () {
  printf "%s" "$1"
}

pn () {
  printf "%s\n" "$1"
}

strip_last_delimiter () {
  LENGTH="$(p "$STATUS_BAR_DELIMITER" | wc -c)"
  p "$1" | rev | cut -c $((LENGTH + 1))- | rev
}

process_bspwm () {
  local status="$1"
  local monitors_status=""

  monitors=( $(p "$status" | sed 's/\:\([mM]\)/ \1/g') )

  for m in "${monitors[@]}"; do
    local m_name="$(p "$m" | cut -d ':' -f 1 | cut -c 2-)"
    local m_focus="$(p "$m" | cut -d ':' -f 1 | cut -c 1)"

    m_status=( $(p "$m" | cut -d ':' -f 2- | sed 's/\:\([LTG]\)/ \1/') )
    desktops=( $(p "${m_status[0]}" | sed 's/\:/ /g') )
    windows_statuses=( $(p "${m_status[1]}" | sed 's/\:/ /g') )

    [[ "$m_focus" == "M" ]] && monitors_status+=" " || monitors_status+=" "

    for d in "${desktops[@]}"; do
      case $d in
        [o]*) monitors_status+="${d:1} ";;
        [OF]*) monitors_status+="${d:1}˚ ";;
        [uU]*) monitors_status+="$d ";;
      esac
    done

    local state=""
    local layout=""
    local flags=""

    for w in "${windows_statuses[@]}"; do
      case $w in
        [T]*) state="${w:1}";;
        [L]*) layout="${w:1}";;
        [G]*) flags="${w}";;
      esac
    done

    monitors_status+="[$state,$layout,$flags] ";
  done

  p "${monitors_status:0: -1}"
}

# TODO: fix
xtitle -sf "TITLE%s\n" > "$STATUS_BAR_FIFO" &

repeat 'DATE' "date +'%d %b %H:%M '  | tr '[:upper:]' '[:lower:]'" 60 > "$STATUS_BAR_FIFO" &

# repeat 'SPOTIFY' 'sexec statuses/spotify.py' 10 > "$STATUS_BAR_FIFO" &
repeat 'CMUS' 'sexec statuses/cmus.sh' 10 > "$STATUS_BAR_FIFO" &
# repeat 'NORDVPN' 'sexec statuses/nordvpn.sh' 30 > "$STATUS_BAR_FIFO" &
# repeat 'PAVOLUME' 'sexec statuses/pavolume.sh' 5 > "$STATUS_BAR_FIFO" &
repeat 'VOLUME' 'sexec statuses/volume.sh' 5 > "$STATUS_BAR_FIFO" &
repeat 'PROCESSES' 'sexec statuses/processes.sh' 30 > "$STATUS_BAR_FIFO" &
repeat 'BATTERY' 'sexec statuses/battery.sh' 60 > "$STATUS_BAR_FIFO" &
# repeat 'ARCH_UPDATES' 'sexec statuses/arch-updates.sh' 3600 > "$STATUS_BAR_FIFO" &
repeat 'NETWORK' 'sexec statuses/network.sh' 20 > "$STATUS_BAR_FIFO" &
repeat 'KEYBOARD_LAYOUT' 'sexec statuses/keyboard-layout.sh' 2 > "$STATUS_BAR_FIFO" &
repeat 'HIDDEN_WINDOWS' 'sexec statuses/hidden-windows.sh' 5 > "$STATUS_BAR_FIFO" &
# repeat 'CONFIGS' 'GIT_DIR=$HOME/.configs.git GIT_WORK_TREE=$HOME sexec statuses/git-status.sh' 30 > "$STATUS_BAR_FIFO" &

while read -r line ; do
	case $line in
    W*) BSPWM="$(process_bspwm "$(p "$line" | cut -c 2-)")";;
    HIDDEN_WINDOWS*) HIDDEN_WINDOWS="$(p "$line" | cut -c 15-)";;
    TITLE*) TITLE="$(p "$line" | cut -c 6-)";;
    DATE*) DATE="$(p "$line" | cut -c 5-)";;
    CMUS*) CMUS="$(p "$line" | cut -c 5-)";;
    SPOTIFY*) SPOTIFY="$(p "$line" | cut -c 8-)";;
    NORDVPN*) NORDVPN="$(p "$line" | cut -c 8-)";;
    PROCESSES*) PROCESSES="$(p "$line" | cut -c 10-)";;
    PAVOLUME*) PAVOLUME="$(p "$line" | cut -c 9-)";;
    VOLUME*) VOLUME="$(p "$line" | cut -c 7-)";;
    BATTERY*) BATTERY="$(p "$line" | cut -c 8-)";;
    NETWORK*) NETWORK="$(p "$line" | cut -c 8-)";;
    ARCH_UPDATES*) ARCH_UPDATES="$(p "$line" | cut -c 13-)";;
    KEYBOARD_LAYOUT*) KEYBOARD_LAYOUT="$(p "$line" | cut -c 16-)";;
    *) pn "Error: $line";;
	esac

  statuses_left=(
    "$BSPWM"
    "$HIDDEN_WINDOWS"
    "$TITLE"
  )
  statuses_right=(
    "$SPOTIFY"
    "$CMUS"
    "$NORDVPN"
    "$CONFIGS"
    "$PROCESSES"
    "$PAVOLUME"
    "$VOLUME"
    "$BATTERY"
    "$NETWORK"
    "$ARCH_UPDATES"
    "$KEYBOARD_LAYOUT"
    "$DATE"
  )
  statuses_left_output=""
  statuses_right_output=""

  for status in "${statuses_left[@]}"; do
    [[ -n "$status" ]] && statuses_left_output+="$status$STATUS_BAR_DELIMITER"
  done

  for status in "${statuses_right[@]}"; do
    [[ -n "$status" ]] && statuses_right_output+="$status$STATUS_BAR_DELIMITER"
  done

  l="$([[ -n "$statuses_left_output" ]] && strip_last_delimiter "${statuses_left_output}")"
  r="$([[ -n "$statuses_right_output" ]] && strip_last_delimiter "${statuses_right_output}")"
  printf "%s\n" "%{Sf}%{l}$l%{r}$r"
  # printf "%s\n" "%{l}${statuses_left_output:0: -5}%{r}${statuses_right_output:0: -5}"

done < "$STATUS_BAR_FIFO"

wait
