#!/usr/bin/env sh

status_bar_cache=/tmp/$USER/status-bar

mkdir -p $status_bar_cache

run () {
  local cmd="$1"
  local cache_time="$2"
  [[ ! -n "$cache_time" ]] && cache_time="5"

  local cache_file="$(printf "%s" "$cmd" | base64)"
  local full_cache_file_path="$status_bar_cache/$cache_file"

  local cache_file_timestamp="$(date --date="$(stat -c %y $full_cache_file_path)" +"%s")"

  (( $(date +"%s") - cache_file_timestamp >= cache_time )) && bash -c "$cmd" > "$full_cache_file_path"

  cat "$full_cache_file_path"
}

statuses=(
  "$(run 'sexec status-bar/spotify.py' 5)"
  "$(run 'sexec status-bar/cmus.sh' 5)"
  "$(run 'sexec status-bar/nordvpn.sh' 30)"
  "$(run 'sexec status-bar/pavolume.sh' 0)"
  "$(run 'sexec status-bar/arch-updates.sh' 360)"
  "$(run 'sexec status-bar/keyboard-layout.sh' 0)"
  "$(date +'%a %b %d, %H:%M')"
)

for status in "${statuses[@]::${#statuses[@]}-1}"; do
  [[ -n "$status" ]] && printf "%s | " "$status"
done

printf "%s" "${statuses[@]: -1:1}"
