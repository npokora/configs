# configs

# TODO
- dim: make it easy and clean, implement vim everywhere
- AI in day-to-day workflow
- use sockets for statuses
- maybe use find instead of fd (utils)
- rewrite psstatus to be more universal
- use three level security
- vifm git support
- sad , search and replace
- use normal authenticator for two-factor-authentication
- https://labzilla.io/blog/force-dns-pihole
- https://docs.radicle.xyz/docs/getting-started
- add cpu/mem usage to panel
- kakoune + fish/zsh
- create a fancy and minimal config versions
- automount usb
- try kakoune lsp, parinfer plugin
- nm script to turn on/off connections
- dim allow any command execution (with score script it can be really usefull)
- implement sort functionality for fzf etc
- remove status bar use popup window

# FIX
- polkit in nixos
- fix lbry-desktop

# IDEAS
- create your own window manager from dwm
- keybinding for PREVIEW DESKTOPS
- matrix bridges


c add -N ~/.local/bin/**
c add -N ~/.local/share/scripts/**
c add -N ~/.config/nixos-system-wide/**
c add -N ~/.config/lutris/**
c add -N ~/.config/bspwm/**
c add -N ~/.config/herbstluftwm/**
c add -N ~/.config/Vieb/erwics/**
c add -N ~/.config/quteapps/**
c add -N ~/.local/share/applications/*-quteapp.desktop
c add -N ~/.local/share/applications/*-erwic.desktop
c add -N ~/.config/kak/colors/*
c add -N ~/.config/kak/bin/*
c add -N ~/.config/kak/*.kak
