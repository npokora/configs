{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = "dim";

  version = "0.2";

  unpackPhase = ":";

  src = fetchurl {
    name = "dim";
    url = "https://gitlab.com/cypper/configs/-/raw/6211d858dafa34069b489b4d05bea9049fe19b1b/.local/bin/dim";
    sha256 = "05yncg00qf2jknlfsi0ckm71kfvsgd112ln8ndw9r045za474vqv";
    executable = true;
  };

  installPhase = "
    mkdir -p $out/bin
    cp ${src} $out/bin/dim
  ";
}

