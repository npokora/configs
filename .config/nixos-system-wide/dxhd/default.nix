{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = "dxhd";

  version = "05.04.2021_02.38";

  src = fetchurl {
    name = "dxhd";
    url = "https://github.com/dakyskye/dxhd/releases/download/${version}/dxhd_amd64";
    sha256 = "1l964c9xjgk34xns22gqsja0d04zd2x7wj4sf55b136h23dkniqr";
    executable = true;
  };

  unpackPhase = ":";

  installPhase = ''
    mkdir -p $out/bin
    cp ${src} $out/bin/dxhd
  '';

  meta = {
    homepage = "https://github.com/dakyskye/dxhd";
    description = "daky's X11 Hotkey Daemon";
  };
}
