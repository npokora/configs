{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec {
  name = "bashtop";

  version = "0.9.25";

  src = fetchFromGitHub {
    owner = "aristocratos";
    repo = "bashtop";
    rev = "v${version}";
    sha256 = "07nlr6vmyb7yihaxj1fp424lmhwkdjl6mls92v90f6gsvikpa13v";
  };

  installFlags = "PREFIX=\${out} VERSION=${version}";

  meta = {
    homepage = "https://github.com/aristocratos/bashtop";
    description = "Linux/OSX/FreeBSD resource monitor";
  };
}
