#! /bin/sh

#########################
###### Logic Helper #####
#########################
index_location=/tmp/hlwm_logic_index
echo 0 > $index_location
function herbstlogic () {
   if [ "$1" == "if" ]; then
      herbstlogic_if "$@"
      exit
   fi

   logic_index=$(cat $index_location)
   local hl_and=0
   local hl_or=0
   local hl_chain=0
   local hl_type=""

   if [[ $1 == *'&'* ]] ; then
      hl_and=1
      hl_type=and   
      logic_symbol="&"
   fi
   if [[ $1 == *'|'* ]] ; then
      hl_or=1
      hl_type=or   
      logic_symbol="|"
   fi
   if [[ $1 == *','* ]] ; then
      hl_chain=1
      hl_type=chain   
      logic_symbol=","
   fi
   local sep_n=$(( hl_and + hl_or + hl_chain ))

   if [ $sep_n -gt 1 ]; then
      echo "Too many seperators"
      exit 1
   elif [ $sep_n == 0 ]; then
      echo "$1"
      exit 0
   fi

   local seperator="$hl_type""$logic_index"
   echo "$hl_type" "$seperator" ${1//"$logic_symbol"/"$seperator"}
   echo $(( logic_index + 1 )) > $index_location
}

function herbstlogic_if () {
   if [ $1 == "if" ]; then
      shift
   else
      echo "not an if statement"
      exit 1
   fi

   local condition=$(herbstlogic "$1") 
   shift

   if [ $1 == "then" ]; then
      shift
   else
      echo "couldn't find mathing \'then\' command, error token $1"
      exit 1
   fi

   local consequence=$(herbstlogic "$1")
   shift

   cons=$(herbstlogic "$condition & $consequence")

   if [ -z $1 ]; then
      echo $cons
      exit 0
   elif [ $1 == "else" ]; then
      shift
   else
      echo "couldn't find parse $1"
      exit 1
   fi

   local else=$(herbstlogic "$@")

   echo $(herbstlogic "$cons | $else")
   exit 0
}

# The old documentation for this script is wrong
