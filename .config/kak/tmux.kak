# add-highlighter shared/sxhkd regions
# add-highlighter shared/sxhkd/header region ^(?![\s#]) $ group
# add-highlighter shared/sxhkd/header/ regex [{}] 0:operator
# add-highlighter shared/sxhkd/header/ regex \b(super|alt|shift|ctrl|Return|XF86\w*|slash|bracket|left|right|_)\b 0:keyword

hook global BufCreate .*tmux.conf %{
  set-option buffer filetype tmux
}

hook global WinSetOption filetype=tmux %{
  require-module sh
  add-highlighter window/ ref sh
}
