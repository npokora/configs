def tmux-send-keys -params ..2 -docstring "
tmux-send-keys <target-pane> <keys>: sends keys to target pane
" %{
    evaluate-commands %sh{
      tmux send-keys -t $1 "$2" Enter
    }
}
