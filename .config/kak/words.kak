def -hidden select-prev-word-start-type1 %{
  exec <a-/>[A-Z][a-z]+|[A-Z]+|[a-z]+<ret><a-n>n<a-:><a-\;>
}
def -hidden select-next-word-start-type1 %{
  exec /[A-Z][a-z]+|[A-Z]+|[a-z]+<ret>n<a-n>
}
def -hidden extend-prev-word-start-type1 %{
  exec <a-:><a-\;><a-?>[A-Z][a-z]+|[A-Z]+|[a-z]+<ret>
}
def -hidden extend-next-word-start-type1 %{
  exec <a-:>?[A-Z][a-z]+|[A-Z]+|[a-z]+<ret>
}

map global normal w :select-next-word-start-type1<ret>
map global normal W :extend-next-word-start-type1<ret>
map global normal w :select-next-word-start-type1<ret>
map global normal W :extend-next-word-start-type1<ret>
map global normal b :select-prev-word-start-type1<ret>
map global normal B :extend-prev-word-start-type1<ret>

