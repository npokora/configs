declare-option -hidden str cheatsheetcmd

define-command -params 1.. cheatsheet-file %{
    set-option global cheatsheetcmd "cheat.sh/%opt{filetype}/%sh{echo $@ | tr ' ' '+'}"
    try %{ delete-buffer! *cheat* }
    edit -scratch *cheat*
    execute-keys "!printf '%%s\n\n' %opt{cheatsheetcmd}; curl %opt{cheatsheetcmd}<ret>"
    # ansi-render
}

define-command -params 1.. cheatsheet %{
    set-option global cheatsheetcmd "cheat.sh/%sh{echo $@ | tr ' ' '+'}"
    try %{ delete-buffer! *cheat* }
    edit -scratch *cheat*
    execute-keys "!printf '%%s\n\n' %opt{cheatsheetcmd}; curl %opt{cheatsheetcmd}<ret>"
    # ansi-render
}
