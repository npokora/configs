add-highlighter shared/sxhkd regions
add-highlighter shared/sxhkd/header region ^(?![\s#]) $ group
add-highlighter shared/sxhkd/header/ regex [{}] 0:operator
add-highlighter shared/sxhkd/header/ regex \b(super|alt|shift|ctrl|Return|XF86\w*|slash|bracket|left|right|_)\b 0:keyword

hook global BufCreate .*sxhkdrc %{
  set-option buffer filetype sxhkd
}

hook global WinSetOption filetype=sxhkd %{
  require-module sh
  add-highlighter window/sh ref sh
  add-highlighter window/sxhkd ref sxhkd
}
