map global user f ':e -existing %sh{ wexec utils files_hidden_select }<ret>'                                                                                                      -docstring 'open fzf searcher'
map global user F ':e -existing %sh{ wexec utils files_hidden_sort_select }<ret>'                                                                                                 -docstring 'open fzf searcher'
map global user c %[:e -existing %sh{ wexec utils working_dirs_files_hidden_select }<ret>]                                    -docstring 'open dotfiles fzf searcher'
map global user d ':e -existing %sh{ GIT_DIR=~/.configs.git wexec utils git_files_select | xargs -r printf "~/%s" }<ret>'                                                         -docstring 'open dotfiles fzf searcher'
map global user D ':e -existing %sh{ GIT_DIR=~/.$USER-configs.git wexec utils git_files_select | xargs -r printf "~/%s" }<ret>'                                                   -docstring 'open private dotfiles fzf searcher'
map global user b %[:b %sh{ wexec "printf '%s' '$kak_buflist' | tr ' ' '\n' | mselect" }<ret>]                                                                                                 -docstring 'open buffer'
map global user e ':e -existing %sh{ wexec fselect $kak_buffile }<ret>'                                                                                                           -docstring 'open file explorer'

map global user N ':enter-user-mode -lock number<ret>'   -docstring 'number locked mode'
map global user C ':enter-user-mode clipboard<ret>'      -docstring 'clipboard mode'
map global user <a-c> ':enter-user-mode cheatsheet<ret>' -docstring 'cheatsheet mode'
map global user <a-f> ':enter-user-mode format<ret>'     -docstring 'format mode'
map global user L ':enter-user-mode -lock line<ret>'     -docstring 'line locked mode'
map global user <a-l> ':enter-user-mode lint<ret>'       -docstring 'lint mode'
map global user <a-L> ':enter-user-mode -lock lint<ret>' -docstring 'lint locked mode'
map global user T ':enter-user-mode tags<ret>'           -docstring 'tags mode'
map global user <a-t> ':enter-user-mode trim<ret>'       -docstring 'trim mode'
map global user g ':enter-user-mode grep<ret>'           -docstring 'grep mode'
map global user G ':enter-user-mode -lock grep<ret>'     -docstring 'grep locked mode'
map global user / ':enter-user-mode search<ret>'         -docstring 'search mode'
map global user <a-e> ':enter-user-mode echo<ret>'       -docstring 'echo mode'
map global user h ':enter-user-mode quick-hook<ret>'       -docstring 'echo quick-hook'
map global user <a-d> ':enter-user-mode cd<ret>'       -docstring 'cd'


map global insert <tab> 'i<esc><gt>h<a-d>i'
map global insert <s-tab> '<a-;><lt>'
# map global normal <tab> '<gt>'
# map global normal <s-tab> '<lt>'

map global normal <c-d> 'vc<c-d>:enter-user-mode goto<ret>5'
map global normal <c-u> 'vc<c-u>:enter-user-mode goto<ret>5'
map global normal <a-q> ':wq<ret>'
map global normal = ':indent-selection<ret>'
map global normal <a-,> ':<up><ret>'
map global normal * '<esc><a-*>'
map global normal <a-*> '<esc><a-i>w<a-*>'
map global normal + '%s<c-r>/<ret>'
map global normal <a-+> '<esc><a-i>w<a-*>%s<c-r>/<ret>'
map global normal <c-n> ':select-under-cursor<ret>'
map global normal <c-a-n> ':select-prev-under-cursor<ret>'
map global normal <c-a> ':select-all-under-cursor<ret>'
map global normal '#' ':comment-line<ret>'                     -docstring '(un)comment line'
map global normal '<a-#>' ':comment-block<ret>'                -docstring '(un)comment block'
map global normal "'" ':enter-user-mode mirror<ret>' -docstring 'mirror locked mode'
map global normal "<a-'>" ':enter-user-mode -lock mirror<ret>'           -docstring 'mirror mode'

# ukrainian
map global normal т 'n'
map global normal Т 'N'
map global normal ф 'a'
map global normal Ф 'A'
map global normal р 'h'
map global normal Р 'H'
map global normal о 'j'
map global normal О 'J'
map global normal л 'k'
map global normal Л 'K'
map global normal д 'l'
map global normal Д 'L'
map global normal к 'r'
map global normal К 'R'
map global normal ш 'i'
map global normal Ш 'I'
map global normal щ 'o'
map global normal Щ 'O'
map global normal г 'u'
map global normal Г 'U'
map global normal ч 'x'
map global normal Ч 'X'
map global normal н 'y'
map global normal Н 'Y'
map global normal в 'd'
map global normal В 'D'
map global normal с 'c'
map global normal С 'C'
map global normal з 'p'
map global normal З 'P'
map global normal п 'g'
map global normal П 'G'
map global normal ц 'w'
map global normal Ц 'W'
map global normal у 'e'
map global normal У 'E'
map global normal и 'b'
map global normal И 'B'
map global normal ь 'm'
map global normal Ь 'M'
map global normal <a-о> '<a-j>'
map global normal ж ';'
map global normal Ж ':'
# map global normal . '/'
# map global normal , '?'
map global normal б ','
map global normal Б '<'

map global goto д 'l'
map global goto Д 'L'
map global goto р 'h'
map global goto Р 'H'
map global goto ш 'i'
map global goto Ш 'I'
map global goto ф 'a'
map global goto Ф 'A'
map global goto л 'k'
map global goto Л 'K'
map global goto о 'j'
map global goto О 'J'

alias global ц write
alias global й quit
alias global цй write-quit
alias global и buffer

define-command -hidden text-object-indented-paragraph %{
  eval -save-regs '^' %{
    execute-keys -draft -save-regs '' '<a-i>pZ'
    execute-keys '<a-i>i<a-z>i'
  }
}
map global object P '<esc>:text-object-indented-paragraph<ret>'

map global view u '<esc><c-u>V'                                    -docstring 'up 50% of buffer'
map global view d '<esc><c-d>V'                                    -docstring 'down 50% of buffer'

map global goto n '<esc>:grep-next-match<ret>'                                                                                                                               -docstring 'grep next match'
map global goto N '<esc>:grep-previous-match<ret>'                                                                                                                           -docstring 'grep prev match'
map global goto 1 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 1 / 10 ))j }"<ret>' -docstring '10% of buffer'
map global goto 2 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 2 / 10 ))j }"<ret>' -docstring '20% of buffer'
map global goto 3 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 3 / 10 ))j }"<ret>' -docstring '30% of buffer'
map global goto 4 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 4 / 10 ))j }"<ret>' -docstring '40% of buffer'
map global goto 5 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 5 / 10 ))j }"<ret>' -docstring '50% of buffer'
map global goto 6 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 6 / 10 ))j }"<ret>' -docstring '60% of buffer'
map global goto 7 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 7 / 10 ))j }"<ret>' -docstring '70% of buffer'
map global goto 8 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 8 / 10 ))j }"<ret>' -docstring '80% of buffer'
map global goto 9 '<esc>:exec "gg%sh{ export kak_window_at=$(echo $kak_window_range | cut -d '' '' -f 1) && echo $(( kak_window_at + kak_window_height * 9 / 10 ))j }"<ret>' -docstring '90% of buffer'
map global goto m '<esc>m;'                                                                                                                                                  -docstring 'matching char'
map global goto d '<esc><a-i>w:ctags-search<ret>'   -docstring 'ctags search'
map global goto v '<esc><a-i>w:ctags-funcinfo<ret>' -docstring 'ctags funcinfo'

declare-user-mode line
map global line J 'J<a-x>'        -docstring 'up extend'
map global line K 'K<a-x>'        -docstring 'down extend'
map global line j 'j<a-x>'        -docstring 'up'
map global line k 'k<a-x>'        -docstring 'down'

declare-user-mode echo
map global echo o ': echo %opt{}<left>'        -docstring 'opt'
map global echo O ': echo -debug %opt{}<left>' -docstring 'opt debug'
map global echo r ': echo %reg{}<left>'        -docstring 'reg'
map global echo R ': echo -debug %reg{}<left>' -docstring 'reg debug'
map global echo s ': echo %sh{}<left>'         -docstring 'sh'
map global echo S ': echo -debug %sh{}<left>'  -docstring 'sh debug'
map global echo v ': echo %val{}<left>'        -docstring 'val'
map global echo V ': echo -debug %val{}<left>' -docstring 'val debug'

declare-user-mode search
map global search / ': exec /<ret>\Q\E<left><left>' -docstring 'regex disabled'
map global search ? ': exec ?<ret>\Q\E<left><left>' -docstring 'regex disabled (extended)'
map global search i '/(?i)'                         -docstring 'case insensitive'
map global search I '?(?i)'                         -docstring 'case insensitive (extended)'

declare-user-mode quick-hook
map global quick-hook t ':prompt -init "tmux-send-keys " quick-hook: ''quick-hook-create BufWritePost %val{text}''<ret>'          -docstring 'tmux-send keys [BufWritePost]'
map global quick-hook s ':prompt -init "nop %%sh{  }" quick-hook: ''quick-hook-create BufWritePost %val{text}''<ret><left><left>' -docstring 'shell cmd [BufWritePost]'

declare-user-mode grep
map global grep g ':prompt grep: ''grep %val{text}; set-register slash "%val{text}"''<ret>'                     -docstring 'grep prompt'
map global grep G ':prompt -init "%reg{slash}" grep: ''grep %val{text}; set-register slash "%val{text}"''<ret>' -docstring 'grep prompt with search value'
map global grep n ':grep-next-match<ret>'                                                                       -docstring 'next match'
map global grep N ':grep-previous-match<ret>'                                                                   -docstring 'prev match'
map global grep a ':e *grep*<ret>'                                                                              -docstring 'show matches'
map global grep r ':grep-apply-changes<ret>'                                                                    -docstring 'apply changes'
map global grep R ':grep-apply-changes -force<ret>'                                                             -docstring 'force apply changes'

declare-user-mode cheatsheet
map global cheatsheet c ':prompt cheatsheet: ''cheatsheet %val{text}; ansi-render''<ret>'      -docstring 'cheatsheet prompt'
map global cheatsheet f ':prompt cheatsheet: ''cheatsheet-file %val{text}; ansi-render''<ret>' -docstring 'cheatsheet file prompt'
map global cheatsheet C ':prompt cheatsheet: ''cheatsheet %val{text}?Q; ansi-render''<ret>'      -docstring 'cheatsheet prompt without comments'
map global cheatsheet F ':prompt cheatsheet: ''cheatsheet-file %val{text}?Q; ansi-render''<ret>' -docstring 'cheatsheet file prompt without comments'
map global cheatsheet <a-c> ':prompt cheatsheet: ''cheatsheet %val{text}?T''<ret>'      -docstring 'cheatsheet prompt without ansi'
map global cheatsheet <a-f> ':prompt cheatsheet: ''cheatsheet-file %val{text}?T''<ret>' -docstring 'cheatsheet file prompt without ansi'
map global cheatsheet <a-C> ':prompt cheatsheet: ''cheatsheet %val{text}?QT''<ret>'      -docstring 'cheatsheet prompt without comments and ansi'
map global cheatsheet <a-F> ':prompt cheatsheet: ''cheatsheet-file %val{text}?QT''<ret>' -docstring 'cheatsheet file prompt without comments and ansi'

declare-user-mode tags
map global tags g ':ctags-generate<ret>' -docstring 'ctags generate'
map global tags s ':ctags-search<ret>'   -docstring 'ctags search'
map global tags f ':ctags-funcinfo<ret>' -docstring 'ctags funcinfo'

declare-user-mode trim
map global trim h '<esc>s^\h+<ret><a-d>'           -docstring '← trim selections content start'
map global trim j '<esc>S^\s+<ret><a-k>\S<ret>'    -docstring '← trim start'
map global trim T '<esc>s^\h+|\h+$<ret><a-d>'      -docstring '↔ trim selections content both'
map global trim t '<esc>_'                         -docstring '↔ trim both'
map global trim k '<esc>S\s+$|\n<ret><a-k>\S<ret>' -docstring '→ trim end'
map global trim l '<esc>s\h+$<ret><a-d>'           -docstring '→ trim selections content end'

declare-user-mode format
map global format j '<esc>`'                          -docstring '↓ lowercase'
map global format k '<esc>~'                          -docstring '↑ uppercase'
map global format s '<esc><a-`>'                      -docstring '↕ switchcase'
map global format f ':format<ret>'                    -docstring 'format'
map global format c ':comment-line<ret>'              -docstring 'comment-line'
map global format C ':comment-block<ret>'             -docstring 'comment-block'
map global format t ':set global indentwidth 0<ret>'  -docstring 'indent tab'
map global format 2 ':set global indentwidth 2<ret>'  -docstring 'indent space 2'
map global format 4 ':set global indentwidth 4<ret>'  -docstring 'indent space 4'

declare-user-mode lint
map global lint d ':lint-disable<ret>'        -docstring 'disable'
map global lint e ':lint-enable<ret>'         -docstring 'enable'
map global lint l ':lint<ret>'                -docstring 'lint'
map global lint n ':lint-next-error<ret>'     -docstring 'next'
map global lint p ':lint-previous-error<ret>' -docstring 'prev'
map global lint b ':b *lint-output*<ret>'     -docstring 'output buffer'

# clipboard
map global normal Y y
map global normal D d
# map global normal y ',Cy' -docstring 'copy to system buffer'
# map global normal d ',Cd' -docstring 'copy to system buffer and delete'
# map global normal p ',Cp' -docstring 'append from system buffer'
# map global normal P ',CP' -docstring 'insert from system buffer'
# map global normal R ',CR' -docstring 'replace from system buffer'

declare-user-mode clipboard
map global clipboard y '<a-|> xsel -i -b<ret>'   -docstring 'copy to system buffer'
map global clipboard d '<a-|> xsel -i -b<ret>d'   -docstring 'copy to system buffer and delete'
map global clipboard p '<a-!>xsel -o -b<ret>' -docstring 'append from system buffer'
map global clipboard P '!xsel -o -b<ret>'     -docstring 'insert from system buffer'
map global clipboard R '|xsel -o -b<ret>'     -docstring 'replace from system buffer'

declare-user-mode number
map global number k '|echo $((kak_selection + 1))<ret>'  -docstring 'increment number by 1'
map global number j '|echo $((kak_selection - 1))<ret>'  -docstring 'decrement number by 1'
map global number K '|echo $((kak_selection + 10))<ret>' -docstring 'increment number by 10'
map global number J '|echo $((kak_selection - 10))<ret>' -docstring 'decrement number by 10'

declare-user-mode mirror

# grow/shrink
map global mirror c 'C<a-;><a-C><a-;>'  -docstring column
map global mirror h 'H<a-;>L<a-;>'      -docstring character
map global mirror l 'L<a-;>H<a-;>'      -docstring character
map global mirror j 'J<a-;>K<a-;>'      -docstring line
map global mirror k 'K<a-;>J<a-;>'      -docstring line
map global mirror J 'J<a-;>K<a-;><a-x>' -docstring 'full line'
map global mirror K 'K<a-;>J<a-;><a-x>' -docstring 'full line'
map global mirror b 'B<a-;>W<a-;>'      -docstring 'word begin'
map global mirror w 'W<a-;>B<a-;>'      -docstring 'word begin'
map global mirror e 'E<a-;>B<a-;>'      -docstring 'word end'
map global mirror p '}p<a-;>{p<a-;>'    -docstring paragraph
map global mirror s '}s<a-;>{s<a-;>'    -docstring sentence
map global mirror m 'M<a-;><a-M><a-;>'  -docstring matching

# insert/delete
map global mirror <space> 'a<space><esc>i<space><esc>H<a-;>'          -docstring '<space>surround<space>'
map global mirror ( 'a)<esc>i(<esc>H<a-;>'          -docstring '(surround)'
map global mirror { 'a}<esc>i{<esc>H<a-;>'          -docstring '{surround}'
map global mirror [ 'a]<esc>i[<esc>H<a-;>'          -docstring '[surround]'
map global mirror < 'a<gt><esc>i<lt><esc>H<a-;>'    -docstring '<surround>'
map global mirror ) 'a )<esc>i( <esc>2H<a-;>'       -docstring '( surround )'
map global mirror } 'a }<esc>i{ <esc>2H<a-;>'       -docstring '{ surround }'
map global mirror ] 'a ]<esc>i[ <esc>2H<a-;>'       -docstring '[ surround ]'
map global mirror > 'a <gt><esc>i<lt> <esc>2H<a-;>' -docstring '< surround >'
map global mirror '"' 'a"<esc>i"<esc>H<a-;>'        -docstring '"surround"'
map global mirror "'" "a'<esc>i'<esc>H<a-;>"        -docstring "'surround'"
map global mirror '`' 'a`<esc>i`<esc>H<a-;>'        -docstring '`surround`'
map global mirror d 'Z<a-S><a-d>z<a-:>H'            -docstring 'delete'

# fallthrough
map global mirror '<a-;>' '<a-;>'       -docstring 'swap anchor and cursor'
map global mirror '<a-S>' '<a-S>'       -docstring 'select sels boundaries'

