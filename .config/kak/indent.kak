def indent-selection %{
  eval -save-regs '"^i' %{
    exec '<a-x>"iZ'
    try %{
      exec '"_s^[ \t]+<ret><a-d>'
    }
    exec '"iz'
    execute-keys -with-hooks "<a-d>ko%sh{ echo ""$kak_selection"" | sed 's/</<lt>/g' }<esc>:select %val{selection_desc}<ret><a-x>"
  }
}
