def quick-hook-create \
  -shell-script-candidates 'printf "BufWritePost\nBufWritePre\nInsertChar\nRuntimeError\nBufReadFifo\nBufReload"' \
  -docstring 'quick-hook-create <hook_name> <hook_command>: allows use to define simple quick hook' \
  -params 2 %{
    remove-hooks window "quick-hook-%arg{1}"
    hook -group "quick-hook-%arg{1}" window "%arg{1}" .* "%arg{2}"
  }
