def jsdoc %{
  try %{
    exec 'ghF(MF{!jsdoc-generate<ret>}<esc>'
    exec '<a-/>/\*\*<ret>?\*/<ret>J<a-s><a-&><space>'
    exec '<a-?>/\*\*<ret>jghhf*l'
  } catch %{
    exec '<space>'
    echo "jsdoc Error: couldn't parse function declaration"
  }
}

hook global WinSetOption filetype=javascript %{
  set-option buffer formatcmd "prettier --stdin-filepath %val{buffile}"
  set-option buffer lintcmd "eslint --format /lib/node_modules/eslint-formatter-kakoune"

  add-highlighter shared/javascript/code/ regex (?<!\?)\s([\w_$]{1}[\w\d_$]*)\s*: 1:variable
  add-highlighter shared/javascript/code/ regex [^\w_$](?!if)(?!for)(?!while)([\w_$]{1}[\w\d_$]*)\s*\( 1:function
  add-highlighter shared/javascript/code/ regex (&&|\|\||\?) 1:operator
  add-highlighter shared/javascript/code/ regex \s(:)\s 1:operator
  add-highlighter shared/javascript/code/ regex \b(console|module|process|Map|Set|WeakMap|WeakSet|Proxy|Promise|Array|Boolean|Date|Function|Number|Object|RegExp|String|Symbol)\b 1:builtin

  map global user d ':jsdoc<ret>' -docstring "Generate jsdoc"
}

hook global WinSetOption filetype=typescript %{
  set-option buffer formatcmd "prettier --stdin-filepath %val{buffile}"
  set-option buffer lintcmd "eslint --format /lib/node_modules/eslint-formatter-kakoune"

  add-highlighter shared/typescript/code/ regex (?<!\?)\s([\w_$]{1}[\w\d_$]*)\s*: 1:variable
  add-highlighter shared/typescript/code/ regex [^\w_$](?!if)(?!for)(?!while)([\w_$]{1}[\w\d_$]*)\s*\( 1:function
  add-highlighter shared/typescript/code/ regex (&&|\|\||\?) 1:operator
  add-highlighter shared/typescript/code/ regex \s(:)\s 1:operator
  add-highlighter shared/typescript/code/ regex \b(console|module|process|Map|Set|WeakMap|WeakSet|Proxy|Promise|Array|Boolean|Date|Function|Number|Object|RegExp|String|Symbol)\b 1:builtin

  map global user d ':jsdoc<ret>' -docstring "Generate jsdoc"
}
