let $INSIDE_VIFM = 'true'
set vifminfo=dhistory,chistory,state,tui,shistory,
    \phistory,fhistory,dirstack,registers,bookmarks,bmarks
set dotfiles
set dotdirs
set relativenumber
set number
set vicmd="sexec bin/kak-vifm"
set tuioptions=psu
colorscheme Dracula
set syscalls
set nofollowlinks
set vimhelp
set wildmenu
set wildstyle=popup
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers
set ignorecase
set smartcase
set nohlsearch
set incsearch
set scrolloff=4
set statusline="   Hint: %z%= %A | %u:%-7g | %s | %d   "

command! fzf :let $TMP_FILE=term('utils files_hidden_select 2> /dev/tty') | if $TMP_FILE != "" | goto $TMP_FILE | endif
command! ag :let $TMP_FILE=term("utils search_files_hidden_select_filename %a 2> /dev/tty") | if $TMP_FILE != "" | goto $TMP_FILE | endif
command! mount :let $MOUNT_DISK=term("ls /dev/s* | fzf 2> /dev/tty") | if $MOUNT_DISK != "" | :execute "!sudo mount $MOUNT_DISK %a -o umask=000 2> /dev/tty" | goto "%a" | endif

nnoremap ,f :fzf<cr>
nnoremap ,S :ag<space>
nnoremap ,s :sort<cr>
nnoremap ,p :view<cr>
nnoremap ,d :!dragon %f<cr>
nnoremap ,ax :!7z x %c -oarchive<cr>
nnoremap ,aa :!7z a archive.zip %f<cr>
vnoremap ,ax :!7z x %c -oarchive<cr>
vnoremap ,aa :!7z a archive.zip %f<cr>

mark h ~
mark m /mnt
mark r /

" Interaction with system clipboard
if has('win')
    " Yank current directory path to Windows clipboard with forward slashes
    nnoremap yp :!echo %"d:gs!\!/! %i | clip<cr>
    " Yank path to current file to Windows clipboard with forward slashes
    nnoremap yf :!echo %"c:gs!\!/! %i | clip<cr>
elseif executable('xsel')
    " Yank current directory path into primary and selection clipboards
    nnoremap yd :!echo -n %d | xsel --input --primary %i &&
                \ echo -n %d | xsel --clipboard --input %i<cr>
    " Yank current file path into into primary and selection clipboards
    nnoremap yf :!echo -n %c:p | xsel --input --primary %i &&
                \ echo -n %c:p | xsel --clipboard --input %i<cr>
elseif executable('xclip')
    " Yank current directory path into the clipboard
    nnoremap yd :!echo %d | xclip %i<cr>
    " Yank current file path into the clipboard
    nnoremap yf :!echo %c:p | xclip %i<cr>
endif

" Others
filetype *,.* xdg-open %f &

fileviewer *.pdf pdftotext -nopgbrk %c -
fileviewer *.mp3 mp3info
fileviewer *.flac soxi
fileviewer *.[1-8] man ./%c | col -b
fileviewer *.bmp,*.jpg,*.jpeg,*.png,*.gif,*.xpm chafa -c 256 -s 60x60 %f
fileviewer *.torrent dumptorrent -v %c
fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zip -sf %c
fileviewer *.tgz,*.tar.gz tar -tzf %c
fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
fileviewer *.tar.txz,*.txz xz --list %c
fileviewer *.tar tar -tf %c
fileviewer *.rar unrar v %c
fileviewer *.7z 7z l %c
fileviewer *.doc catdoc %c
fileviewer *.docx docx2txt.pl %f -
fileviewer *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
          \*.fl[icv],*.m2v,*.mov,*.webm,*.mts,*.m4v,*.r[am],*.qt,*.divx,
          \*.as[fx]
         \ ffprobe -pretty %c 2>&1

