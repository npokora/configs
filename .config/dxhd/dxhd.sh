#!/bin/sh

export CURRENT_WM="$(sexec get-current-wm.sh)"

################
######## GENERAL
################

### super + a; {a,n,l,p,o,t,k,K,c,C,j,N,w,W}
##dim {actions,network-manager,launcher,pass,app-launcher,translate,fkill,fkill-unknown-or-bad,calendar,clipboard-edit,japanese,notes,nodes,weather}

# super + a
bexec sexec launchers/hotkeys.sh window

# super + d
sexec launchers/actions.sh window

# super + p
sexec launchers/launcher.sh window

# super + {_,shift + }Return
## {TMUX= alacritty,vieb}
{$TERMINAL,sexec qutebrowser-quick-launch}

# XF86MonBrightness{Up,Down}
light -{A,U} 15

# super + XF86MonBrightness{Up,Down}
for monitor_name in $(sexec monitors.sh names); do xrandr --output $monitor_name --brightness {1, 0.5}; done


## # XF86Audio{RaiseVolume,LowerVolume}
## sexec audio-control.sh volume-change {5, -5}
## 
## # XF86AudioMute
## sexec audio-control.sh volume-toggle
## 
## # XF86{AudioPlay,Forward,Back}
## sexec media-control.sh {play-pause,next,previous}
## 
## # super + F{1,2,3}
## sexec media-control.sh {play-pause,next,previous}

## super + o ; {e,w,m}
##   {gvim,firefox,thunderbird}

## # super + alt + l
## bexec sexec i3-lock-with-blur.sh;

## # super + u
## sexec status-bars/refresh-status-bar.sh

###########
######## WM
###########

###
### wm hotkeys
###

# super + slash
case $CURRENT_WM in
  "awesome") awesome-client 'require("awful").layout.inc(1)';;
  "bspwm") sexec launchers/nodes.sh window;;
esac

# super + shift + slash
case $CURRENT_WM in
  "awesome") awesome-client 'require("awful").layout.inc(1)';;
esac

# super + alt + q
case $CURRENT_WM in
  "bspwm") bspc quit;;
  "qtile") qtile-cmd -o cmd -f shutdown;;
esac

# super + alt + r
case $CURRENT_WM in
  "bspwm") bspc wm -r;;
  "qtile") qtile-cmd -o cmd -f restart;;
esac

###
### state/flags
###

# super + {_, shift + }Tab
case $CURRENT_WM in
  "qtile") qtile-cmd -o cmd -f {next,prev}_layout;;
esac

# super + shift + q
case $CURRENT_WM in
  "bspwm") bspc node -c;;
  "qtile") qtile-cmd -o cmd window -f kill;;
esac

# super + f
case $CURRENT_WM in
  "awesome") awesome-client 'require("awful").client.fullscreen.toggle()';;
  "bspwm") bspc node -t ~fullscreen;;
  "qtile") qtile-cmd -o cmd window -f toggle_fullscreen;;
esac

# super + shift + f
case $CURRENT_WM in
  "awesome") awesome-client 'require("awful").client.floating.toggle()';;
  "bspwm") bspc node -t ~floating;;
  "qtile") qtile-cmd -o cmd window -f toggle_floating;;
esac

## set the window state
# super + ctrl + shift + {t,u}
case $CURRENT_WM in
  "bspwm") bspc node -t {tiled,pseudo_tiled};;
esac

## set the node flags
# super + ctrl + shift + {m,l,s,p}
case $CURRENT_WM in
  "bspwm") bspc node -g {marked,locked,sticky,private};;
esac

## mininize node
# super + m
case $CURRENT_WM in
  "bspwm") bspc node -g hidden;;
esac

## unmininize last node
# super + shift + m
case $CURRENT_WM in
  "bspwm")
    export BSPWM_HIDDEN_NODE=`bspc query -N -n any.hidden`;
    bspc node $BSPWM_HIDDEN_NODE -g hidden=off -n '@/1';
    bspc node -f $BSPWM_HIDDEN_NODE;
  ;;
esac

###
### focus/swap
###

## focus the next/previous monitor
# super + bracket{right,left}
if [[ "{right,left}" == "right" ]]; then
  BSPWM="next"; AWESOME="-1"
else
  BSPWM="prev"; AWESOME="1"
fi

case $CURRENT_WM in
  "awesome") awesome-client "require(\"awful\").screen.focus_relative($AWESOME)";;
  "bspwm") bspc monitor -f $BSPWM;;
esac

## focus the next/previous node in the current desktop
# super + {_,shift + }{period,comma}
case $CURRENT_WM in
  "bspwm") bspc node -{f,s} {next,prev}.local.leaf;;
esac

## focus the last node in the current desktop
# super + {i,o}
case $CURRENT_WM in
  "bspwm") bspc node -f {older,older} ;;
esac

## swap the last node in the current desktop
# super + shift + {i,o}
case $CURRENT_WM in
  "bspwm") bspc node -s {older,older} --follow;;
esac

## focus or send to the given desktop
# super + shift + {1-9,0}
TAG={1-9,0};
case $CURRENT_WM in
  "bspwm") bspc node -d "$TAG.local";;
  "qtile") qtile-cmd -o cmd window -f togroup -a $TAG;;
esac

# super + {1-9,0}
TAG={1-9,0};
case $CURRENT_WM in
  "bspwm") bspc desktop -f "$TAG.local";;
  "qtile") qtile-cmd -o cmd group $TAG -f toscreen;;
esac

## TODO: preview desktops
## super + F{1-9}
##   VAR="1" && echo "$VAR"

## super + @F{1-9}
##   echo "$VAR"

## focus or send the next/previos monitor
# super + shift + bracket{right,left}
case $CURRENT_WM in
  "bspwm") bspc node -m {next,prev};;
esac

## focus the node for the given path jump
# super + {_,shift +} equal
case $CURRENT_WM in
  "bspwm") bspc node -f @{parent,second};;
esac

## focus the node in the given direction
# super + h
case $CURRENT_WM in
  "bspwm") bspc node -f west;;
  "qtile") qtile-cmd -o cmd layout -f left;;
esac

## focus the node in the given direction
# super + j
case $CURRENT_WM in
  "bspwm") bspc node -f south;;
  "qtile") qtile-cmd -o cmd layout -f down;;
esac

## focus the node in the given direction
# super + k
case $CURRENT_WM in
  "bspwm") bspc node -f north;;
  "qtile") qtile-cmd -o cmd layout -f up;;
esac

## focus the node in the given direction
# super + l
case $CURRENT_WM in
  "bspwm") bspc node -f east;;
  "qtile") qtile-cmd -o cmd layout -f right;;
esac

## swap the node in the given direction
# super + shift + h
case $CURRENT_WM in
  "bspwm") bspc node -s west --follow;;
  "qtile") qtile-cmd -o cmd layout -f shuffle_left;;
esac

## swap the node in the given direction
# super + shift + l
case $CURRENT_WM in
  "bspwm") bspc node -s east --follow;;
  "qtile") qtile-cmd -o cmd layout -f shuffle_right;;
esac

## swap the node in the given direction
# super + shift + j
case $CURRENT_WM in
  "bspwm") bspc node -s south --follow;;
  "qtile") qtile-cmd -o cmd layout -f shuffle_down;;
esac

## swap the node in the given direction
# super + shift + k
case $CURRENT_WM in
  "bspwm") bspc node -s north --follow;;
  "qtile") qtile-cmd -o cmd layout -f shuffle_up;;
esac

## swap the current node and the biggest node
# super + {_,shift + }b
case $CURRENT_WM in
  "bspwm") bspc node -{f,s} biggest.local;;
esac

## Rotate all nodes in root in the focused desktop
# super + shift + {p,n}
case $CURRENT_WM in
  "bspwm") bspc node '@/' -C {backward,forward};;
esac


###
### move/resize
###

## expand a window by moving one of its side outward
## super + alt + {h,j,k,l}
##   case $CURRENT_WM in
##     "bspwm") bspc node -z {right -20 0,bottom 0 20,bottom 0 -20,right 20 0};;
##   esac

## equalize all windows in the focused desktop
# super + alt + e
case $CURRENT_WM in
  "bspwm") bspc node '@/' -E;;
  "qtile") qtile-cmd -o cmd layout -f normalize;;
esac

## rotate root node in the focused desktop
# super + alt + t
case $CURRENT_WM in
  "bspwm") bspc node '@/' -R -90 && bspc node '@/' -E;;
esac

## Expand/contract a window by moving one of its side outward/inward
## super + r : {h,j,k,l}
##STEP=20; SELECTION={1,2,3,4};
##case $CURRENT_WM in
##  "bspwm")
##    bspc node -z $(echo "left -$STEP 0,bottom 0 $STEP,top 0 -$STEP,right $STEP 0" | cut -d',' -f$SELECTION) ||
##    bspc node -z $(echo "right -$STEP 0,top 0 $STEP,bottom 0 -$STEP,left $STEP 0" | cut -d',' -f$SELECTION)
##  ;;
##esac

## super + r : e
##case $CURRENT_WM in
##  "bspwm") bspc node '@/' -E;;
##esac

# super + ctrl + {j,k}
case $CURRENT_WM in
  "qtile") qtile-cmd -o cmd layout -f {shrink,grow};;
esac

###
### preselect
###

## preselect the direction
# super + ctrl + shift + {h,j,k,l}
case $CURRENT_WM in
  "bspwm") bspc node -p {west,south,north,east};;
esac


## preselect the ratio
# super + ctrl + shift + {1-9}
case $CURRENT_WM in
  "bspwm") bspc node -o 0.{1-9};;
esac


## cancel the preselection for the focused node
# super + ctrl + shift + {q,c}
case $CURRENT_WM in
  "bspwm") bspc node -p {cancel,cancel};;
esac

## move window to preselected area
# super + ctrl + shift + i
case $CURRENT_WM in
  "bspwm") bspc node -n last.!automatic;;
esac

## move window to root
# super + ctrl + b
case $CURRENT_WM in
  "bspwm") bspc node -n @/;;
esac

## flips the window
# super + ctrl {_,+ shift} + f
case $CURRENT_WM in
  "bspwm") bspc node @parent -R {90,-90};;
esac

### cancel the preselection for the focused desktop,
##super + ctrl + shift + space
## bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

###
### tabs
###

## Add focused window to a tabbed instance in given direction
# super + ctrl + {h,j,k,l}
case $CURRENT_WM in
  "bspwm") sexec tabs.sh $(bspc query -N -n {west,south,north,east}) add $(bspc query -N -n focused);;
esac


## Remove one tab from tabbed
# super + ctrl + {q,c}
case $CURRENT_WM in
  "bspwm")
    tabbed=$(bspc query -N -n {focused,focused});
    child=$(sexec tabs.sh $tabbed list | head -n1);
    sexec tabs.sh $tabbed remove $child
  ;;
esac


###
### bspwm hotkeys
###


### alternate between the tiled and monocle layout
##super + m
## bspc desktop -l next

### send the newest marked node to the newest preselected node
##super + y
## bspc node newest.marked.local -n newest.!automatic.local

###
### state/flags
###


###
### focus/swap
###


### focus the last node/desktop
##super + {grave,Tab}
## bspc {node,desktop} -f last

### focus the older or newer node in the focus history
##super + {o,i}
## bspc wm -h off;
## bspc node {older,newer} -f;
## bspc wm -h on

###
### move/resize
###

### contract a window by moving one of its side inward
##super + alt + shift + {h,j,k,l}
## bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

### move a floating window
##super + {Left,Down,Up,Right}
## bspc node -v {-20 0,0 20,0 -20,20 0}

