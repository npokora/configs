from ranger.api.commands import Command
import sys
import os
import re

class f(Command):
    def execute(self):
        import subprocess
        import os.path
        if self.quantifier:
            # match only directories
            command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
                    -o -type d -print 2> /dev/null | sed 1d | cut -b3- | f +m"
        else:
            # match files and directories
            command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
                    -o -print 2> /dev/null | sed 1d | cut -b3- | f +m"
        fzf = self.fm.execute_command(command, universal_newlines=True, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.rstrip('\n'))
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.select_file(fzf_file)

class s(Command):
  def execute(self):
    import subprocess
    import os.path
    command = 's ' + ' '.join(self.args[1:]) + ' | f --ansi'
    ag = self.fm.execute_command(command, universal_newlines=True, stdout=subprocess.PIPE)
    stdout, stderr = ag.communicate()

    if ag.returncode == 0:
      choice = stdout.rstrip('\n')
      ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
      ag_file_path = ansi_escape.sub('', choice.split(':')[0])
      ag_file = os.path.abspath(ag_file_path)
      if os.path.isdir(ag_file):
        self.fm.cd(ag_file)
      else:
        self.fm.select_file(ag_file)
