#!/usr/bin/env sh

source "$XDG_CONFIG_HOME/env"

mkdir -p "$XDG_DATA_HOME"/wineprefixes
mkdir -p "$XDG_CACHE_HOME"/less

mkdir -p "$(dirname $STATUS_BAR_FIFO)"
[ ! -e "$STATUS_BAR_FIFO" ] && mkfifo "$STATUS_BAR_FIFO"

setopt share_history
setopt inc_append_history
setopt hist_ignore_all_dups
setopt complete_aliases

# powerline
if [[ -x "$(command -v starship)" ]]; then
  eval "$(starship init zsh)"
else
  function git_status {
    git_dirty=$(git rev-parse 2>/dev/null && (git diff --no-ext-diff --quiet --exit-code 2> /dev/null || echo -e \•))
    git_dirty_staged=$(git rev-parse 2>/dev/null && (git diff --cached --no-ext-diff --quiet --exit-code 2> /dev/null || echo -e \•))
    git_branch=$(git symbolic-ref --short HEAD 2>/dev/null | cut -c -15)
    if ! [ "$git_branch" == "" ]; then
      echo -e "\001\033[01;35m\002$git_branch\001\033[00m\002\001\033[01;31m\002$git_dirty\001\033[00m\002\001\033[01;32m\002$git_dirty_staged\001\033[00m\002 "
    fi
  }
  export PS1="\$(git_status)\[\033[01;34m\]\w \[\033[00m\]"
fi

# vi mode in bash
set -o vi

bindkey '^P' history-search-backward
bindkey '^N' history-search-forward

autoload -U +X compinit && compinit
autoload -U +X bashcompinit && bashcompinit

source "$XDG_CONFIG_HOME"/zsh/utils.zsh

# autocompletion
# if [[ "$OSTYPE" == "darwin"* ]]; then
#   if [ -f $(brew --prefix)/etc/bash_completion ]; then
#     . $(brew --prefix)/etc/bash_completion
#   fi
# else
#   if [ -f /etc/bash_completion ]; then
#     . /etc/bash_completion
#   fi
#   if [ -f $SYSTEM_SHARE_DIR/bash-completion/bash_completion ]; then
#     source $SYSTEM_SHARE_DIR/bash-completion/bash_completion
#   fi
# fi

# source $XDG_CONFIG_HOME/broot/launcher/bash/br

source "$XDG_CONFIG_HOME/aliases"

compdef c='git'
compdef p='git'
compdef g='git'

ask_to_run_startx () {
  answer="$(echo -e "yes\nno" | mselect 'printf "Do you want to run startx?"')"

  [[ "$answer" == "yes" ]] && sexec bin/jdm start
  [[ "$answer" == "no" ]] && clear
}

if [ "$TERM" = "linux" ]; then
  echo -en "\e]P0101010" #black
  echo -en "\e]P82B2B2B" #darkgrey
  echo -en "\e]P1D75F5F" #darkred
  echo -en "\e]P9E33636" #red
  echo -en "\e]P287AF5F" #darkgreen
  echo -en "\e]PA98E34D" #green
  echo -en "\e]P3D7AF87" #brown
  echo -en "\e]PBFFD75F" #yellow
  echo -en "\e]P48787AF" #darkblue
  echo -en "\e]PC7373C9" #blue
  echo -en "\e]P5BD53A5" #darkmagenta
  echo -en "\e]PDD633B2" #magenta
  echo -en "\e]P65FAFAF" #darkcyan
  echo -en "\e]PE44C9C9" #cyan
  echo -en "\e]P7E5E5E5" #lightgrey
  echo -en "\e]PFFFFFFF" #white
  clear #for background artifacting

  ask_to_run_startx
fi

if [ -f ~/.profile ]; then
    . ~/.profile
fi

# [[ ! -n "$TMUX" ]] && tmux-tmp
